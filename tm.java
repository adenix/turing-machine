/*******************************************************************************
** @author Austin Nicholas
** @version 1.0.3
*******************************************************************************/

import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

// -----[ chomsky ]-------------------------------------------------------------
// Description:     The class chomsky is used to create Grammar objects and test
//                  if specific strings are in the grammar.
public class tm {

  // -----[ ANSI Colors ]
  public static final String ANSI_RESET = "\u001B[0m";
  public static final String ANSI_YELLOW = "\u001B[33m";
  public static final String ANSI_CYAN = "\u001B[36m";

  // -----[ File IO ]
  // INSTRUCTION: To run program with a different input file you must change
  //              change the name here.
  private static final String INPUT_FILE = "input.in";

  // ------[ main ]------------------------------------
  public static void main(String[] args) {

    // -----[ VAR DEC/INIT ]
    Scanner in;
    int numMachines;

    try {
      // TASK: Open INPUT_FILE
      in = new Scanner(new File(INPUT_FILE));
    } catch(FileNotFoundException e) {
      // TASK: OUTPUT ERROR if INPUT_FILE is not found
      System.out.printf("\n%s>  FILE NOT FOUND: '%s%s%s'%s\n", ANSI_YELLOW,
        ANSI_CYAN,INPUT_FILE, ANSI_YELLOW, ANSI_RESET);
      System.out.printf("%s>  VERBOS: %s", ANSI_YELLOW, ANSI_RESET);
      System.out.println(ANSI_CYAN + e + ANSI_RESET);
      System.out.println();

      System.exit(0);

      // ERROR SUPPRESSOR: LN50 - variable in might not have been initialized
      in = null;
    } // try catch block

    numMachines = in.nextInt();

    // TASK: Run for each machine
    for(int i = 0; i < numMachines; i++) {

      // TASK: Output test case
      System.out.println("Machine #" + (i+1) + ":");
      // TASK: Create Turing Machine Object
      Turing turing = new Turing(in);

      // IN: Number of Test cases
      int numTests = in.nextInt();
      // IN: Number of Trys before halting machine
      int numTrys = in.nextInt();

      // TASK: Run for each test case
      for(int j = 0; j < numTests; j++) {
        // IN: Test String
        String test = in.next();
        // TASK: Run algorithm for tests
        int result = turing.test(test,numTrys);

        // TASK: Display results
        System.out.println(test + ": " +
          ((result == 1)? "YES" :
          ((result == -1)? ("DOES NOT HALT IN " + numTrys + " STEPS") : "NO")));
      }

      // Line seperator
      System.out.println();
    }

    // TASK: Close INPUT_FILE
    in.close();
  } // main
}

// -----[ Turing ]--------------------------------------------------------------
// Description:     Creates a turing machine object used to test if strings can
//                  be represented by the machine.
class Turing {

  private final int ACCEPT = 1;
  private final int REJECT = 2;
  private final int POUND = Integer.valueOf('#');
  private final int RIGHT = Integer.valueOf('R');
  private final int LEFT = Integer.valueOf('L');

  // -----[ VAR DEC/INIT ]
  int numStates, numRules;
  int rules[][];

  public Turing(Scanner in) {

    this.numStates = in.nextInt();
    this.numRules = in.nextInt();

    rules = new int[numStates][64];

    for(int i = 0; i < numRules; i++) {

      // rules[STATE_AT][CHAR_IN] = HEX(STATE_TO, CHAR_REPLACE, DIRECTION)
      rules[in.nextInt()][Integer.valueOf(in.next().charAt(0)) - POUND] =
          toHex(  in.nextInt(),
                  Integer.valueOf(in.next().charAt(0)),
                  Integer.valueOf(in.next().charAt(0)));
    }
  }

  // -----[ test ]-----------------------------------
  // Description:     Test to see if a string in in the language described by
  //                  the Turring Machine.
  public int test(String test, int stepLim) {

    // -----[ VAR DEC/INIT ]
    ArrayList<Character> testAL = new ArrayList<Character>();
    int position = 0, state = 0;

    // TASK: Convert string into ArrayList
    for (char c : test.toCharArray())
      testAL.add(c);

    while(stepLim > 0) {

      stepLim--;

      // TASK: Retreive rule baised on the state and readin value
      int[] rule =
        fromHex(rules[state][Integer.valueOf(testAL.get(position)) - POUND]);

      if(rule[0] != -1) {
        state = rule[0];
        testAL.set(position, (char)rule[1]);

        // TASK: Move the head pointer left or right
        if(rule[2] == RIGHT)
          position++;
        else if(rule[2] == LEFT)
          position--;

        // TASK: Simulate the head not moving if it is trying to go left at
        //       position 0. Grow the arraylist if position = the arraylist size
        if(position == -1)
          position = 0;
        else if(position == testAL.size())
          testAL.add('B');

      }

      if(state == ACCEPT)
        return 1;
      else if(state == REJECT)
        return 0;
    }
    return -1;
  }

  // -----[ toHex ]-----------------------------------
  // Description:     Convert three integers into one hexidecimal value.
  //
  // Return:          Hexidecimal value storing three integer values.
  private int toHex(int stateTo, int replace, int direction) {
    return (stateTo << 16) | (replace << 8) | direction;
  }

  // -----[ fromHex ]-----------------------------------
  // Description:     Convert one hexidecimal value into three integers.
  //
  // Return:          Array of three integer values.
  private int[] fromHex(int hex) {

    if(hex == 0)
      return new int[]{-1};;

    return new int[]{hex >> 16, (hex >> 8) & 0xFF, hex & 0xFF};
  }
}
